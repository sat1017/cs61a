# About
These are my solutions to CS61A Fall 2020 homework problems.
Below is the course website and list of completed subjects.

<https://cs61a.org/>

## Homework
- [X] HW 01: Variables & Functions, Control
- [X] HW 02: Recursion
- [X] HW 03: Trees, Data Abstraction
- [X] HW 04: Nonlocal, Iterators
- [X] HW 05: Object-Oriented Programming, Linked Lists, Trees
- [X] HW 06: Scheme
- [X] HW 07: Scheme Lists
- [ ] HW 08: Scheme
- [X] HW 09: SQL

## Lectures
- [X] Lecture 01: Computer Science
- [X] Lecture 02: Functions
- [X] Lecture 03: Control
- [X] Lecture 04: Higher-Order Functions
- [X] Lecture 05: Environments
- [X] Lecture 06: Design
- [X] Lecture 07: Function Examples
- [X] Lecture 08: Recursion
- [X] Lecture 09: Tree Recursion
- [X] Lecture 10: Containers
- [X] Lecture 11: Data Abstraction
- [X] Lecture 12: Trees
- [X] Lecture 13: Binary Numbers
- [X] Lecture 14: Circuits
- [X] Lecture 15: Mutable Values
- [X] Lecture 16: Mutable Functions
- [X] Lecture 17: Iterators
- [X] Lecture 18: Objects
- [X] Lecture 19: Inheritance
- [X] Lecture 20: Representation
- [X] Lecture 21: Composition
- [X] Lecture 22: Efficiency
- [X] Lecture 23: Decomposition
- [X] Lecture 24: Data Examples
- [X] Lecture 25: Users
- [X] Lecture 26: Ethical AI & Data
- [X] Lecture 27: Scheme
- [X] Lecture 28: Exceptions
- [X] Lecture 29: Interpreters
- [X] Lecture 30: Declarative Programming
- [X] Lecture 31: Tables
- [X] Lecture 32: Aggregation
- [X] Lecture 33: Databases
- [ ] Lecture 34: Tail Calls
- [ ] Lecture 35: Macros